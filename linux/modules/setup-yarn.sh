#!/bin/bash

# Installation and setup of Yarn

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh

# Setup
$PACKAGE_INSTALLER "yarn"
curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo

